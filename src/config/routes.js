import Vue from 'vue';
import VueRouter from "vue-router";

import JRecipesView from "../view/JRecipesView";
import JLoginView from "../view/JLoginView";

Vue.use(VueRouter);

const routes = [{
    name: 'receitas',
    path: '/',
    component: JRecipesView,
}, {
    name: 'login',
    path: '/login',
    component: JLoginView,
}];

export default new VueRouter({
    mode: 'history',
    routes,
});

