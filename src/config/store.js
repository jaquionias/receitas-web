import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        token: '',
        config: {
            baseUrlApi: (process.env.VUE_APP_AMBIENTE == 'development') ? process.env.VUE_APP_BASE_URL : '',
        },
        user: null,
    },
    mutations: {
        setUser(state, user) {
            state.user = user;
            if (user) {
                state.token = user.token;
            }else{
                state.token = '';
            }
        },
    },
    getters: {
        getHeaderAuth: state => {
            state;
            let options = {
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${state.token}`,
                },
            };
            return options;
        },
        getHeader: state => {
            state;
            let options = {
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                },
            };
            return options;
        },
    },
});