import 'font-awesome/css/font-awesome.css';
import Vue from 'vue';
import axios from 'axios';
import App from './App.vue';
import VueSweetalert2 from 'vue-sweetalert2';

import VueHtmlToPaper from 'vue-html-to-paper';

const options = {
    name: '_blank',
    specs: [
        'fullscreen=yes',
        'titlebar=yes',
        'scrollbars=yes'
    ],
    styles: [
        'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css',
        'https://unpkg.com/kidlat-css/css/kidlat.css'
    ]
}

import store from "./config/store";
import router from './config/routes';

import {
    ValidationObserver,
    ValidationProvider,
    extend,
    localize
} from "vee-validate";

import pt_BR from "vee-validate/dist/locale/pt_BR.json";
import * as rules from "vee-validate/dist/rules";

import BootstrapVue from 'bootstrap-vue';
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

Object.keys(rules).forEach(rule => {
    extend(rule, rules[rule]);
});

localize("pt_BR", pt_BR);

Vue.component("ValidationObserver", ValidationObserver);
Vue.component("ValidationProvider", ValidationProvider);

Vue.use(BootstrapVue);
Vue.use(VueSweetalert2);
Vue.use(VueHtmlToPaper, options);

window.eventBus = new Vue();
Vue.config.productionTip = false;
Vue.prototype.$axios = axios;


new Vue({
    store,
    router,
    render: h => h(App),
}).$mount('#app');
